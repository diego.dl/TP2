const BASE_API_URL = "http://localhost:1234/api";
let form = document.getElementById("form");

form.addEventListener('submit', (e)=>{
	e.preventDefault();
	let result = document.getElementById("result"),
		search = form.search.value,
		url = BASE_API_URL + "?action=opensearch&format=json&formatversion=2&search=" + search +"&namespace=0&limit=10&suggest=true";
	console.log(url);
	let req = new XMLHttpRequest();
	req.onreadystatechange = () =>{
		if(req.readyState == 4 && req.status == 200){
			result.innerHTML = "<h3>Résultats:</h3>";
			let jsonResult = (JSON.parse(req.responseText))[1];
			for(let i = 0; i < jsonResult.length; i++){
				result.innerHTML += "<p>"+jsonResult[i]+"</p>";
			}
		}
	}
	req.open("GET", url, true);
	req.send();
});